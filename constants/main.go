package main

//constants:
//are immutable, but can be shadowed
//are replaced by the compiler at compile time
// - we cannot calculate and assign values to them at runtime from a function
//named like variables - PascalCase for exported constants, camelCase for internal constants
//typed constants work like immutable variables - can only interoperate with the same type
//untyped constants work like literals - can work with int8/16/32/64 or uint8/16/32/64

//enumerated constants:
// - special symbol "iota" allows related constants to be created easily
// - iota starts at 0 in each const block, and increments by one
// - it's a good practice to throw away the zero value - we need to watch out for bugs like when
// constant values match zero values for unassigned variables

//enumerated expressions using the iota operator:
// - operations can be determined at compile time are allowed,
// like arithmetic, bitwise operations, and bitshifting

import (
	"fmt"
)

//iota showcase
const a1 = iota
const b1 = iota
const c1 = iota

const (
	_  = iota //throwaway variable that cannot be accessed
	a2 = iota
	b2 = iota + 2
	c2 // subsequent unassigned constants will also be iota + 2
	d2
)

//bitshifting by iota showcase
const (
	_  = iota
	KB = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
	ZB
	YB
)

//assigning permissions in a single byte of data
const (
	isAdmin = 1 << iota
	isHQ
	canSeeFinancials

	canSeeAfrica
	canSeeAsia
	canSeeEurope
	canSeeNorthAmerica
	canSeeSouthAmerica
)

func main() {
	const myConst = 100 // the correct type will be decided by the compiler
	var a int8 = 110
	fmt.Printf("%v, %T\n", myConst+a, myConst+a) //will overflow

	fmt.Printf("%v %v %v\n", a1, b1, c1)
	fmt.Printf("%v, %v, %v, %v\n", a2, b2, c2, d2)
	fileSize := 1200000000.
	fmt.Printf("%.2fGB, %T\n", fileSize/GB, fileSize)

	//some roles
	var roles byte = isAdmin | canSeeNorthAmerica | canSeeFinancials | canSeeEurope // 01100101(x_2) == 101(x_10)
	fmt.Printf("%b\n", roles)
	//bitwise maths with bitmasking
	fmt.Printf("Is Admin? %v & %v is %v\n", roles, isAdmin, roles&isAdmin == isAdmin)
	fmt.Printf("Can see Asia? %v & %v is %v\n", roles, canSeeAsia, roles&canSeeAsia == canSeeAsia)

}
