package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"
)

// Leetcode 1: Two Sum in Go
func TwoSum(nums []int, target int) []int {
	hashmap := make(map[int]int)
	for i, val := range nums {
		_, ok := hashmap[val]
		if ok {
			return []int{hashmap[target-nums[i]], i}
		}
		hashmap[target-nums[i]] = i
	}
	return nil
}

const rawResp = `
{
    "header": {
        "code": 0,
        "message": ""
    },
    "data": [{
        "type": "user",
        "id": 100,
        "attributes": {
            "email": "bob@yandex.ru",
            "article_ids": [10, 11, 12]
        }
    }]
} 
	`

type (
	Response struct {
		Header ResponseHeader `json:"header"`
		Data   ResponseData   `json:"data,omitempty"`
	}

	ResponseHeader struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}

	ResponseData []ResponseDataItem

	ResponseDataItem struct {
		Type       string                `json:"type"`
		Id         int                   `json:"id"`
		Attributes ResponseDataItemAttrs `json:"attributes"`
	}

	ResponseDataItemAttrs struct {
		Email      string `json:"email"`
		ArticleIds []int  `json:"article_ids"`
	}
)

func ReadResponse(rawResp string) (Response, error) {
	resp := Response{}
	if err := json.Unmarshal([]byte(rawResp), &resp); err != nil {
		return Response{}, fmt.Errorf("JSON Unmarshal error: %w", err)
	}
	return resp, nil
}

func main() {
	prices := map[string]int{
		"bread":     50,
		"sosig":     500,
		"pork":      900,
		"ham":       700,
		"cheese":    600,
		"cucumbers": 200}
	for k, v := range prices {
		if v > 500 {
			fmt.Println("Delicacy", k, "costs", v)
		}
	}
	order := []string{"bread", "pork", "cheese", "cucumbers"}
	sum := 0
	for _, v := range order {
		sum += prices[v]
	}
	fmt.Println("Order sums to", sum)

	target := 10
	nums := []int{1, 3, 5, 13, 9, 11}
	fmt.Println(TwoSum(nums, target))

	type Person struct {
		Name        string    `json:"name"`
		Email       string    `json:"email"`
		DateOfBirth time.Time `json:"-"` // this field will not be serialized
	}

	man := Person{
		Name:        "Alex",
		Email:       "alex@yandex.ru",
		DateOfBirth: time.Now(),
	}
	jsMan, err := json.Marshal(man)
	if err != nil {
		log.Fatalln("Bad marshal to json")
	}
	fmt.Printf("Man %v\n", string(jsMan)) // Man {"Имя":"Alex","Почта":"alex@yandex.ru"}

	resp, err := ReadResponse(rawResp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", resp)
}
