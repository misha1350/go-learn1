package main

import "fmt"

// pointers are used for:
// 1. when you want to change the value of a variable from a called function
// if you pass a variable by value, all modifications within the function will apply
// to the local copy and leave the original variable unchanged (due to shadowing)

// 2. when you need to emphasize that a value may be missing, for example -
// if there's a function that returns a record of a user (with a struct) by its identifier
// the pointer returning the result will show that not all identifiers can be found for the user

// 3. when you work with resources like file descriptors or sockets,
// copying such variables may result in exhaustion of system resources or not being copied at all

// 4. when you're working with large variables and copying across the stack takes more time and
// resources than having the pointer garbage data collected by the compiler

// pointers should not be used for:
// 1. when you want to speed up your app and it seems that copying structures can be too expensive
// unless there are tests showing that pointers improve performance, don't spend time to optimize
// you are likely to waste your time and sanity, or decrease the system's performance
// by increasing the costs of garbage collection

// 2. when the structure, which you want tor replace values in using pointers, doesn't take up
// around 100 bytes or more

// 3. when a lot of pointers in memory put a heavy load on the garbage collector
// this can happen when you create your own in-memory database

func main() {
	i := 42
	p := &i
	fmt.Println(*p) // returns value of "i" by dereferencing "p" pointer
	*p = 21         // assign value to "i" through "p"
	fmt.Println(*p)

	type A struct {
		IntField int
	}

	p2 := &A{}       // could also be done with p2 := new(A)
	p2.IntField = 27 // same as (*p2).IntField = 27
	fmt.Println(p2.IntField)

	// pointers use-case #1

	incrementCopy := func(i2 int) {
		i2++
	}

	increment := func(i2 *int) {
		(*i2)++
	}

	i2 := 43

	incrementCopy(i2)
	fmt.Println(i2) // 43

	increment(&i2)
	fmt.Println(i2) // 44

}
