package main

import (
	"fmt"
)

func main() {
	//arrays have a fixed length but their values can be changed
	//unlike in Python, they can only contain values of a matching filetype
	var grades [3]int = [3]int{97, 85, 93}
	fmt.Printf("1.\t%v, %T\n", grades, grades)

	var students [5]string
	students[0] = "Lisa"
	students[3] = "Panagiotis"
	fmt.Printf("2.\tStudents: %v\n", students)
	fmt.Printf("3.\tNumber of students: %v\n", len(students))

	//2D Array
	var matrix = [3][3]int{
		{1, 0, 0},
		{0, 1, 0},
		{0, 0, 1}}
	fmt.Println("4.\t", matrix)

	a := [...]int16{1, 5, 123}
	b := a
	//pointers point at an address of a variable and are not copies of the variable
	//useful when passing an array to a function
	c := &a
	b[1] = 7
	fmt.Println(a)
	fmt.Println(b)
	c[1] = 12
	fmt.Println(a, cap(a))
	fmt.Println(c)

	//slices are built on arrays but have variable length, we can append data to them
	//slices are naturally referenced variables, you can pass them through to a function without using a pointer
	var subjects = []string{"1. Maths", "2. English", "3. Chemistry", "4. PE"}
	fmt.Println(subjects, len(subjects), cap(subjects))
	subjects = append(subjects, "5. Physics", "6. History") //creates a new slice with more allocated capacity
	fmt.Println(subjects, len(subjects), cap(subjects))
	subjects = append(subjects, "7. IT", "8. Spanish", "9. CRT")
	//looks like a larger array's capacity would be 2x the previous array's capacity
	fmt.Println(subjects, len(subjects), cap(subjects))

	//slice ranges
	rangeOne := subjects[1:4]  //will return 2nd, 3rd and 4th objects of a slice
	rangeTwo := subjects[5:]   //will return objects from 6th to last
	rangeThree := subjects[:2] //will return objects from 1st to 2nd
	fmt.Println(rangeOne, rangeTwo, rangeThree)
	//rangeFour := subjects[1:4:9]
	//fmt.Println(rangeFour, cap(rangeFour))

	temp := subjects
	temp[3] = "123. Crimes Against Humanity"
	//changes both arrays - watch out for this when you have multiple slices pointing to the same data
	fmt.Println(temp)
	fmt.Println(subjects)

}
