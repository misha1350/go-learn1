package main

import (
	"fmt"
	"strconv"
)

func main() {
	for i := 1; i <= 100; i++ {
		var str string
		if i%3 == 0 {
			str += "Fizz"
		}
		if i%5 == 0 {
			str += "Buzz"
		}
		// if i%7 == 0 {
		// 	str += "Fuzz"
		// }
		if str == "" {
			str = strconv.Itoa(i)
		}
		fmt.Println(str)
	}

}
