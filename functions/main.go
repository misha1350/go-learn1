package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
)

type figures int

const (
	square figures = iota
	circle
	triangle
)

func double(x int) int {
	return x + x
}

func add(left, right int) int {
	return left + right
}

func area(f figures) (func(float64) float64, bool) {
	switch f {
	case square:
		return func(x float64) float64 { return x * x }, true
	case circle:
		return func(x float64) float64 { return 3.142 * x * x }, true
	case triangle:
		return func(x float64) float64 { return 0.433 * x * x }, true
	default:
		return nil, false
	}
}

func main() {
	fmt.Println("A dozen is", double(6))
	PrintAllFilesWithFilterClosure(".", ".git")
	ar, ok := area(circle)
	if ok {
		fmt.Println("Area is", ar(2))
	} else {
		fmt.Println("Unknown figure type")
	}
	fmt.Println(Global)
	UseGlobal()
	fmt.Println("After defer:", Global)
}

func PrintAllFiles(path string) {
	// get list of all elements in folder (both files and dirs)
	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println("unable to get list of files", err)
		return
	}
	// go through the list
	for _, f := range files {
		// get filename
		// filepath.Join gathers names and returns /path/to/file
		filename := filepath.Join(path, f.Name())
		// print out name of an element
		fmt.Println(filename)
		// if an element is a directory, call the function recursively
		if f.IsDir() {
			PrintAllFiles(filename)
		}
	}
}

func PrintAllFilesWithFilter(path, filter string) {
	// get list of all elements in folder (both files and dirs)
	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println("unable to get list of files", err)
		return
	}
	for _, f := range files {
		filename := filepath.Join(path, f.Name())
		if strings.Contains(filename, filter) {
			fmt.Println(filename)
		}
		if f.IsDir() {
			PrintAllFilesWithFilter(filename, filter)
		}
	}
}

// using closures, the unchanged parameter would not be copied at each recursive call
// the closure just references its value, thus increasing the speed of the program
// and reducing the chance of an error occuring there
func PrintAllFilesWithFilterClosure(path, filter string) {
	// get list of all elements in folder (both files and dirs)
	// we create it beforehand, not using ':=', so that the closure can refer to it
	var walk func(string)
	walk = func(path string) {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			fmt.Println("unable to get list of files", err)
			return
		}
		for _, f := range files {
			filename := filepath.Join(path, f.Name())
			if strings.Contains(filename, filter) {
				fmt.Println(filename)
			}
			if f.IsDir() {
				walk(filename)
			}
		}
	}
	// now we call walk()
	walk(path)
}

// we can pass in functions as parameters of functions too
// the second parameter takes in an argument and checks if it is a correct type of filter
func PrintAllFilesWithFuncFilter(path string, predicate func(string) bool) {
	var walk func(string)
	walk = func(path string) {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			fmt.Println("unable to get list of files", err)
			return
		}
		for _, f := range files {
			filename := filepath.Join(path, f.Name())
			// print out name of an element if predicate() returns true
			if predicate(filename) {
				fmt.Println(filename)
			}
			if f.IsDir() {
				walk(filename)
			}
		}
	}
	walk(path)
}

// defer showcase
var Global = 12

// changing a global variable's value and setting it back to normal
func UseGlobal() {
	defer func(checkout int) {
		Global = checkout
	}(Global) // we pass the current Global value into the deferred lambda function
	Global = 13
	fmt.Println("Changed global variable:", Global)
	// the deferred function will be called here
}
