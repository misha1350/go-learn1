package main

import (
	"bufio"
	"fmt"
	"os"
)

// this app showcases pointers

func f(cnt *int) {
	*cnt++
}

func main() {
	// get the reader of the user's input
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Interaction counter")

	cnt := 0
	for {
		fmt.Print("-> ")
		// the app waits for user's inputted string to read
		_, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}

		f(&cnt)

		fmt.Printf("User input %d lines\n", cnt)
	}
}
